# base image. for this program node js so
FROM node

#create dir in docker container
RUN mkdir /app
#change dir
WORKDIR /app

#use copy to copy files from local to container
#you can use ADD however ADD extracs archives
#this file only need for install application
COPY package.json /app
#yarn is app for node coders
RUN yarn install

COPY . /app
RUN yarn build

#public port 3000 (for this app) 
EXPOSE 3000

#exec after start container
CMD yarn start
#you can use entrypoint instead CMD
#to run docker run -it "laskjdflkajsdf" start

#ENTRYPOINT yarn
